# Plaintext data files and data generators

Lists of nouns, verbs, names, and other stuff you can use to generate shit for whatever you want.

## Resources:

* [Desiquintans Great Noun List](http://www.desiquintans.com/nounlist)
* [Verbinator](http://verbinator.com/)
* [List of Verbs, Nouns, Adjectives, Adverbs - ashley-bovan.co.uk](http://www.ashley-bovan.co.uk/words/partsofspeech.html)
* [SCOWL (Spell Checker Oriented Word Lists)](http://wordlist.aspell.net/)
* [WordNet / Princeton](https://wordnet.princeton.edu/download/current-version)
