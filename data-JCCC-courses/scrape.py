import requests
import csv
from bs4 import BeautifulSoup

pages = [
  "https://catalog.jccc.edu/coursedescriptions/aac/",
  "https://catalog.jccc.edu/coursedescriptions/acct/",
  "https://catalog.jccc.edu/coursedescriptions/asl/",
  "https://catalog.jccc.edu/coursedescriptions/ani/",
  "https://catalog.jccc.edu/coursedescriptions/anth/",
  "https://catalog.jccc.edu/coursedescriptions/arch/",
  "https://catalog.jccc.edu/coursedescriptions/art/",
  "https://catalog.jccc.edu/coursedescriptions/arth/",
  "https://catalog.jccc.edu/coursedescriptions/astr/",
  "https://catalog.jccc.edu/coursedescriptions/aet/",
  "https://catalog.jccc.edu/coursedescriptions/auto/",
  "https://catalog.jccc.edu/coursedescriptions/biol/",
  "https://catalog.jccc.edu/coursedescriptions/bus/",
  "https://catalog.jccc.edu/coursedescriptions/blaw/",
  "https://catalog.jccc.edu/coursedescriptions/bot/",
  "https://catalog.jccc.edu/coursedescriptions/chem/",
  "https://catalog.jccc.edu/coursedescriptions/coll/",
  "https://catalog.jccc.edu/coursedescriptions/coms/",
  "https://catalog.jccc.edu/coursedescriptions/cdtp/",
  "https://catalog.jccc.edu/coursedescriptions/cis/",
  "https://catalog.jccc.edu/coursedescriptions/cpca/",
  "https://catalog.jccc.edu/coursedescriptions/cs/",
  "https://catalog.jccc.edu/coursedescriptions/cmgt/",
  "https://catalog.jccc.edu/coursedescriptions/co/",
  "https://catalog.jccc.edu/coursedescriptions/cj/",
  "https://catalog.jccc.edu/coursedescriptions/ds/",
  "https://catalog.jccc.edu/coursedescriptions/dhyg/",
  "https://catalog.jccc.edu/coursedescriptions/diet/",
  "https://catalog.jccc.edu/coursedescriptions/draf/",
  "https://catalog.jccc.edu/coursedescriptions/econ/",
  "https://catalog.jccc.edu/coursedescriptions/educ/",
  "https://catalog.jccc.edu/coursedescriptions/elte/",
  "https://catalog.jccc.edu/coursedescriptions/elec/",
  "https://catalog.jccc.edu/coursedescriptions/ems/",
  "https://catalog.jccc.edu/coursedescriptions/engr/",
  "https://catalog.jccc.edu/coursedescriptions/engl/",
  "https://catalog.jccc.edu/coursedescriptions/eap/",
  "https://catalog.jccc.edu/coursedescriptions/entr/",
  "https://catalog.jccc.edu/coursedescriptions/evrn/",
  "https://catalog.jccc.edu/coursedescriptions/fash/",
  "https://catalog.jccc.edu/coursedescriptions/fms/",
  "https://catalog.jccc.edu/coursedescriptions/fire/",
  "https://catalog.jccc.edu/coursedescriptions/flr/",
  "https://catalog.jccc.edu/coursedescriptions/fl/",
  "https://catalog.jccc.edu/coursedescriptions/game/",
  "https://catalog.jccc.edu/coursedescriptions/geos/",
  "https://catalog.jccc.edu/coursedescriptions/gist/",
  "https://catalog.jccc.edu/coursedescriptions/gdes/",
  "https://catalog.jccc.edu/coursedescriptions/hc/",
  "https://catalog.jccc.edu/coursedescriptions/hcis/",
  "https://catalog.jccc.edu/coursedescriptions/hci/",
  "https://catalog.jccc.edu/coursedescriptions/avho/",
  "https://catalog.jccc.edu/coursedescriptions/hvac/",
  "https://catalog.jccc.edu/coursedescriptions/hist/",
  "https://catalog.jccc.edu/coursedescriptions/hon/",
  "https://catalog.jccc.edu/coursedescriptions/hort/",
  "https://catalog.jccc.edu/coursedescriptions/hmgt/",
  "https://catalog.jccc.edu/coursedescriptions/hmpb/",
  "https://catalog.jccc.edu/coursedescriptions/hum/",
  "https://catalog.jccc.edu/coursedescriptions/it/",
  "https://catalog.jccc.edu/coursedescriptions/itmd/",
  "https://catalog.jccc.edu/coursedescriptions/isap/",
  "https://catalog.jccc.edu/coursedescriptions/jour/",
  "https://catalog.jccc.edu/coursedescriptions/lead/",
  "https://catalog.jccc.edu/coursedescriptions/li/",
  "https://catalog.jccc.edu/coursedescriptions/law/",
  "https://catalog.jccc.edu/coursedescriptions/libr/",
  "https://catalog.jccc.edu/coursedescriptions/mkt/",
  "https://catalog.jccc.edu/coursedescriptions/math/",
  "https://catalog.jccc.edu/coursedescriptions/mirm/",
  "https://catalog.jccc.edu/coursedescriptions/mfab/",
  "https://catalog.jccc.edu/coursedescriptions/mus/",
  "https://catalog.jccc.edu/coursedescriptions/ndt/",
  "https://catalog.jccc.edu/coursedescriptions/nurs/",
  "https://catalog.jccc.edu/coursedescriptions/phil/",
  "https://catalog.jccc.edu/coursedescriptions/phot/",
  "https://catalog.jccc.edu/coursedescriptions/hper/",
  "https://catalog.jccc.edu/coursedescriptions/psci/",
  "https://catalog.jccc.edu/coursedescriptions/phys/",
  "https://catalog.jccc.edu/coursedescriptions/plum/",
  "https://catalog.jccc.edu/coursedescriptions/pols/",
  "https://catalog.jccc.edu/coursedescriptions/pn/",
  "https://catalog.jccc.edu/coursedescriptions/psyc/",
  "https://catalog.jccc.edu/coursedescriptions/ph/",
  "https://catalog.jccc.edu/coursedescriptions/rrtc/",
  "https://catalog.jccc.edu/coursedescriptions/rrel/",
  "https://catalog.jccc.edu/coursedescriptions/rrit/",
  "https://catalog.jccc.edu/coursedescriptions/rrt/",
  "https://catalog.jccc.edu/coursedescriptions/rdg/",
  "https://catalog.jccc.edu/coursedescriptions/rel/",
  "https://catalog.jccc.edu/coursedescriptions/rc/",
  "https://catalog.jccc.edu/coursedescriptions/sci/",
  "https://catalog.jccc.edu/coursedescriptions/soc/",
  "https://catalog.jccc.edu/coursedescriptions/sag/",
  "https://catalog.jccc.edu/coursedescriptions/thea/",
  "https://catalog.jccc.edu/coursedescriptions/web/",
  "https://catalog.jccc.edu/coursedescriptions/wgs/",
]

# Some info from:
# https://www.geeksforgeeks.org/python-web-scraping-tutorial/

header = [ "CODE", "TITLE", "HOURS", "DESCRIPTION" ]
data = []

plaintextFile = open( "courses.txt", "w" )

for page in pages:
  print( "Pulling", page, "..." )
  webRequest = requests.get( page )
  
  soup = BeautifulSoup( webRequest.content, "html.parser" )
  
  # One course is listed in "courseblock" class (div).
  # The class code is in a link with the class "contentFlip" (a).
  # The rest of the class title is under a (span).
  # The course description is under a paragraph with class "courseblockdesc" (p).
  
  courseBlocks = soup.find_all( "div", class_="courseblock" )
  
  for course in courseBlocks:
    try:
      findCode = course.find( "a", class_="contentFlip" )
      code = findCode.text.strip()
      
      findTitle = course.find( "span" )
      title = findTitle.text.strip()
      
      findDesc = course.find( "p", class_="courseblockdesc" )
      desc = findDesc.text.strip()
      
      # Contains the (a) for title, (span) for title, the # of hours isn't surrounded in anything
      findHours = course.find( "strong" )
      hours = findHours.text.replace( code, "" ).replace( title, "" ).replace("(", "").replace(")", "").strip()
      
      record = [ code, title, hours, desc ]
      data.append( record )
      
      # Also write out as plaintext
      plaintextFile.write( code + "\n" )
      plaintextFile.write( title + "\n" )
      plaintextFile.write( hours + "\n" )
      plaintextFile.write( desc + "\n" )
      plaintextFile.write( "----" + "\n" )
      
    except:
      print( "Some kind of error occurred." )
  
# Output CSV File
# https://www.pythontutorial.net/python-basics/python-write-csv-file/

with open( "courses.csv", "w", encoding="UTF8", newline="\n" ) as csvFile:
  writer = csv.writer( csvFile )
  writer.writerow( header )
  writer.writerows( data )
