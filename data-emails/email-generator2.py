import random
import string
import datetime

# Words to derive email addresses from
names = []

# Dumb host names
hosts = []

used = []
emails = []

def ReadFiles():
    with( open( "../data-websites/websites.txt" ) ) as fileHandle:
        lines = fileHandle.readlines()
    for line in lines:
        line = line.strip('\n').lower()
        hosts.append( line )
        
    with( open( "../data-names/data-names.txt" ) ) as fileHandle:
        lines = fileHandle.readlines()
    for line in lines:
        line = line.strip('\n').replace( ' ', '-' ).lower()
        names.append( line )

def GenerateEmail():
    initialCount = random.randint( 1, 10 )
    
    randinitial = ""
    # Add randinitial
    for i in range( initialCount ):
        randinitial = random.choice( string.ascii_letters ).lower()
    
    # Add number
    randnum1 = str( random.randint( 0, 999999 ) )
        
    # Add name
    randname = random.choice( names )

    # Add number
    randnum2 = str( random.randint( 0, 999999 ) )
    
    # Add host
    randhost = "@" + random.choice( hosts ) 
    
    email1 =    randinitial + randnum1 + randname + randnum2 + randhost
    email2 =    randname + randnum1 + randinitial + randnum2 + randhost
    email3 =    randinitial + randnum1 + randnum2 + randname + randhost
    email4 =    randname + randinitial + randnum1 + randnum2 + randhost
    email5 =    randinitial + randname + randnum1 + randnum2 + randhost
    
    return email1, email2, email3, email4, email5

def GetRandomEmails():
    email1, email2, email3, email4, email5 = GenerateEmail()   
    
    while ( email1 in used ):
        email1, email2, email3, email4, email5 = GenerateEmail() 
    
    used.append( email1 )

    return email1, email2, email3, email4, email5

ReadFiles()

# Create list

itemCount = 1000000 # int( input( "How many items? " ) )

out = open( "emails4.txt", "w" )

counter = 0
emailsPerGeneration = 5
while ( counter < itemCount ):
    if ( counter % 10000 == 0 ):
        now = datetime.datetime.now()
        currentTime = now.strftime("%H:%M:%S")
        print( currentTime + "\t Getting item " + str( counter ) + "-" + str( counter + emailsPerGeneration ) + "/" + str( itemCount ) )
        
    email1, email2, email3, email4, email5 = GetRandomEmails()
    out.write( email1 + "\n" )
    out.write( email2 + "\n" )
    out.write( email3 + "\n" )
    out.write( email4 + "\n" )
    out.write( email5 + "\n" )
    
    counter += emailsPerGeneration
    
out.close()
