import requests
from bs4 import BeautifulSoup
import csv
import time

# Reference: https://realpython.com/beautiful-soup-web-scraper-python/
URL = "https://grownative.org/native-plant-database/?_per_page=-1"
page = requests.get( URL )

soup = BeautifulSoup( page.content, "html.parser" )

data = []

plant_divs = soup.find_all( "div", class_="fwpl-col" )

for single_plant in plant_divs:
    print( "--------------------------------------------------------------" )
    print( single_plant )
    # Find the link within
    subdivs = single_plant.find_all( "fwpl-item" )
    
    for div in subdivs:
        print( "-------------------------------------" )
        print( "DIV:", div )

    linkies = single_plant.find_all( "a" )

    for link in linkies:
        print( )
        print( "LINK:", link )
        print( "HREF:", link.get("href") )
        print( "TEXT:", link.text )
        
        if ( link.text != "" ):
            plant_info = {}
            plant_info = {}
            plant_info[ "plant" ] = link.text
            plant_info[ "link" ] = link.get("href")
            
            # Grab information from this page
            
            sub_page = requests.get( link.get( "href" ) )
            sub_soup = BeautifulSoup( sub_page.content, "html.parser" )
            
            div_scientific = sub_soup.find( "div", class_="et_pb_text_2_tb_body" )
            if ( div_scientific ):
                plant_info[ "scientific" ] = div_scientific.find( "div", class_="et_pb_text_inner" ).get_text()
                                                                                                                                                                                                       
            try:         plant_info[ "type" ]        = sub_soup.find( "div", "et_pb_text_3_tb_body"  ).find( "div", class_="et_pb_text_inner" ).get_text().replace( "<b>Plant Type:</b>", "" )            
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "environment" ] = sub_soup.find( "div", "et_pb_text_4_tb_body"  ).find( "div", class_="et_pb_text_inner" ).get_text().replace( "<b>Native Environment:</b>", "" )   
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "season" ]      = sub_soup.find( "div", "et_pb_text_5_tb_body"  ).find( "div", class_="et_pb_text_inner" ).get_text().replace( "<b>Season of Interest:</b>", "" )   
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "sun" ]         = sub_soup.find( "div", "et_pb_text_12_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                               
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "moisture" ]    = sub_soup.find( "div", "et_pb_text_14_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                               
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "attracts" ]    = sub_soup.find( "div", "et_pb_text_16_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                               
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "benefit" ]     = sub_soup.find( "div", "et_pb_text_18_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                               
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "resistance" ]  = sub_soup.find( "div", "et_pb_text_20_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                               
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "height" ]      = sub_soup.find( "div", "et_pb_text_24_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                               
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "height" ]      += " " + sub_soup.find( "div", "et_pb_text_25_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                        
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "spread" ]      = sub_soup.find( "div", "et_pb_text_28_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                               
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "spread" ]      += " " + sub_soup.find( "div", "et_pb_text_29_tb_body" ).find( "div", class_="et_pb_text_inner" ).get_text()                                        
            except:      print( "Missing field, oh well." )
            try:         plant_info[ "description" ] = sub_soup.find( "div", "et_pb_text_20_tb_body" ).find( "div", class_="et_pb_post_content_0_tb_body" ).find("p").get_text()                         
            except:      print( "Missing field, oh well." )
        
            print( "\n SCRAPED DATA:" )
            print( plant_info )
            data.append( plant_info )
        
            time.sleep( 1 ) # Don't overwhelm the server
           


# <div class="fwpl-item el-lvr66"><a href="https://grownative.org/native_plants/goats-beard/" target="_blank"><img loading="lazy" decoding="async" width="150" height="150" src="https://grownative.org/wp-content/uploads/2020/05/goat-s-beard-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" /></a></div>


# Write data out to csv file
fields = [ "plant", "link", "scientific", "description", "type", "environment", "season", "sun", "moisture", "attracts", "benefit", "resistance", "height", "spread" ]
csv_file = open( "output.csv", "w" )
writer = csv.DictWriter( csv_file, fieldnames=fields )
writer.writeheader()
writer.writerows( data )




