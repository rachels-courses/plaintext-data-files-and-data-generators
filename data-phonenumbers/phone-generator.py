import random

itemCount = int( input( "How many items? " ) )

out = open( "phone-numbers.txt", "w" )

for i in range( 0, itemCount ):
    phone = ""
    
    phone += "("
    for n in range( 3 ):
        phone += str( random.randint( 0, 9 ) )
    
    phone += ")"
    
    for n in range( 3 ):
        phone += str( random.randint( 0, 9 ) )
        
    phone += "-"
    
    for n in range( 4 ):
        phone += str( random.randint( 0, 9 ) )
    
    out.write( phone + "\n" )
