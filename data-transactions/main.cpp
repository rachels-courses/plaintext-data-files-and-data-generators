#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <ctime>
#include <map>
using namespace std;

vector<string> GenerateEmails( int amount )
{
    vector<string> emails;

    vector<string> animals;
    vector<string> adjectives;
    vector<string> businesses;
    vector<string> tld = { ".com", ".net", ".org", ".io" };

    string buffer;
    ifstream fanimals( "../data-animals/data-animals.txt" );
    while ( getline( fanimals, buffer ) )
    {
        animals.push_back( buffer );
    }
    fanimals.close();

    ifstream fadjects( "../data-wordtype-adjectives/data-adjectives.txt" );
    while ( getline( fadjects, buffer ) )
    {
        adjectives.push_back( buffer );
    }
    fadjects.close();

    ifstream fbuzzwords( "../data-companies/companies.txt" );
    while ( getline( fbuzzwords, buffer ) )
    {
        businesses.push_back( buffer );
    }
    fbuzzwords.close();


    for ( int i = 0; i < amount; i++ )
    {
        string name = adjectives[ rand() % adjectives.size() ]
            + animals[ rand() % animals.size() ] + "@"
            + businesses[ rand() % businesses.size() ]
            + tld[ rand() % tld.size() ];

        int pos = name.find( " " );
        while ( pos != string::npos )
        {
            name = name.replace( pos, 1, "" );
            pos = name.find( " " );
        }

        emails.push_back( name );
    }

    return emails;
}

int main()
{
    srand( time( NULL ) );

    vector<string> emails = GenerateEmails( 200 );
    map<string, bool> isInQueue;

    ofstream output( "transactions.txt" );

    for ( int i = 0; i < 1000; i++ )
    {
        int index = rand() % emails.size();

        // Are they in the queue yet?
        if ( isInQueue.find( emails[index] ) != isInQueue.end() )
        {
            // In the queue list
            isInQueue[ emails[index] ] = !isInQueue[ emails[index] ];
        }
        else
        {
            // Put them in the queue
            isInQueue[ emails[index] ] = true;
        }

        if ( isInQueue[ emails[index] ] )
        {
            output << "QUEUE_JOIN " << emails[index] << endl;
        }
        else
        {
            output << "QUEUE_LEFT " << emails[index] << endl;
        }
    }


    return 0;
}
