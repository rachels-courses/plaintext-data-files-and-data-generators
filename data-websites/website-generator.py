import random



# Words to derive from
topics = []
networks = []
tlds = []

used = []

def ReadFiles():
    with( open( "web-topics.txt" ) ) as fileHandle:
        lines = fileHandle.readlines()
    for line in lines:
        line = line.strip('\n').lower()
        topics.append( line )
        
    with( open( "web-networking.txt" ) ) as fileHandle:
        lines = fileHandle.readlines()
    for line in lines:
        line = line.strip('\n').lower()
        networks.append( line )
        
    with( open( "tlds.txt" ) ) as fileHandle:
        lines = fileHandle.readlines()
    for line in lines:
        line = line.strip('\n').lower()
        tlds.append( line )




itemCount = int( input( "How many items? " ) )

ReadFiles()

out = open( "websites.txt", "w" )

for i in range( 0, itemCount ):
    website = ""
    website += topics[ random.randint( 0, len(topics)-1 ) ]
    website += networks[ random.randint( 0, len(networks)-1 ) ]
    website += tlds[ random.randint( 0, len(tlds)-1 ) ]
    
    while ( website in used ):
        website = ""
        website += topics[ random.randint( 0, len(topics)-1 ) ]
        website += networks[ random.randint( 0, len(networks)-1 ) ]
        website += tlds[ random.randint( 0, len(tlds)-1 ) ]

    out.write( website + "\n" )
